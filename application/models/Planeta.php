<?php
  class Planeta extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("planeta",
                $datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    function obtenerTodos(){
      $listadoPlanetas=$this->db->get("planeta");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoPlanetas->num_rows()>0) { //
        return $listadoPlanetas->result();
      }else{
        return false;
      }

    }
    function borrar($id_recup_id)
    {

      $this->db->where("id_recup_id",$id_recup_id);

      if ($this->db->delete("planeta")) {
        return true;
      } else {
        return false;
      }

    }

    //FUNCION PARA CONSULTAR UN PLANETA
    function obtenerPorId($id_recup_id){
      $this->db->where("id_recup_id", $id_recup_id);
      $planeta=$this->db->get("planeta");
      if ($planeta->num_rows()>0) {
        return $planeta->row();
      } else {
        return false;
      }
    }
      //FUNCION PARA ACTUALIZAR UN PLANETA
      function actualizar($id_recup_id,$data){
        $this->db->where("id_recup_id",$id_recup_id);
        return $this->db->update('planeta',$data);
      }
  }//Cierre de la clase

 ?>
