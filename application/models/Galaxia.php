<?php
  class Galaxia extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un Galaxia en MYSQL
    function insertar($datos){
        return $this->db->insert("galaxia",$datos);
    }
    //Funcion para consultar Galaxias
    function obtenerTodos(){
      $listadoGalaxias=
      $this->db->get("galaxia");
      if($listadoGalaxias->num_rows()>0){ //si hay datos
      return $listadoGalaxias->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar galaxia
    function borrar($id_recup_id){
      $this->db->where("id_recup_id",$id_recup_id);
      if ($this->db->delete("galaxia")) {
        return true;
      } else {
        return false;
      }

    }
    //funcion para consultar un instructor especifico
    function obtenerPorId($id_recup_id){
      $this->db->where("id_recup_id",$id_recup_id);
      $galaxia=$this->db->get("galaxia");
      if ($galaxia->num_rows()>0) {
        return $galaxia->row();
      }
      return false;
    }
    //funcion para actualizar un instructor
    function actualizar($id_recup_id,$datos){
      $this->db->where("id_recup_id",$id_recup_id);
      return $this->db->update('galaxia',$datos);
    }
  }//Cierre de la clase

 ?>
