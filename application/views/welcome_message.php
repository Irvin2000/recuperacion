<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
</head>
</body>
<div id="universe-carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicadores -->
    <ol class="carousel-indicators">
      <li data-target="#universe-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#universe-carousel" data-slide-to="1"></li>
      <li data-target="#universe-carousel" data-slide-to="2"></li>
      <!-- Agrega más indicadores según el número de imágenes -->
    </ol>

    <!-- Slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="https://concepto.de/wp-content/uploads/2014/08/universo-e1551279319307-800x400.jpg" alt=""width="80%">
        <div class="carousel-caption">
          <h3>Universo 1</h3>
        </div>
      </div>
      <div class="item">
        <img src="https://www.educaciontrespuntocero.com/wp-content/uploads/2020/07/descubre-los-planetas-del-sistema-solar-a-pluton-y-a-otro-gran-desconocido-5dd39d7dc65b3.jpg" alt=""width="80%">
        <div class="carousel-caption">
          <h3>Universo 2</h3>
        </div>
      </div>
      <div class="item">
        <img src="https://img2.rtve.es/i/?w=1600&i=1684767202038.jpg" alt="" width="80%">
        <div class="carousel-caption">
          <h3>Universo 3</h3>
        </div>
      </div>
      <!-- Agrega más slides según el número de imágenes -->
    </div>

    <!-- Controles -->
    <a class="left carousel-control" href="#universe-carousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#universe-carousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div>
</html>
