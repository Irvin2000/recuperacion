<h1 class="text-center" style="color:white; background-color:gray ; padding: 10px 20px"> <i class="glyphicon glyphicon-globe"></i> NUEVO PLANETA</h1>
<form class=""
id="frm_nuevo_planeta"
action="<?php echo site_url(); ?>/planetas/guardar"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="col-md-3">
          <label for="">Nombre:
<span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del planeta"
          class="form-control"
          name="nombre_recup_id" value=""
          id="nombre_recup_id">
      </div>
      <div class="col-md-4">
          <label for="">Orden:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="number"
          placeholder="Ingrese el orden en el que se encuentra"
          class="form-control" required
          name="orden_recup_id" value=""
          id="orden_recup_id">
      </div>
      <div class="col-md-3">
        <label for="">Distancia:
          <span class="obligatorio">*</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese la distancia en KM"
        class="form-control" required
        name="distancia_recup_id" value=""
        id="distancia_recup_id">
      </div>
      <div class="col-md-1">

      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="col-md-3">
          <label for="">Estado:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el estado del planeta"
          class="form-control" required
          name="estado_recup_id" value=""
          id="estado_recup_id">
      </div>
      <div class="col-md-4">
          <label for="">Galaxia:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type=""
          placeholder="Ingrese la galaxia en la que se encuentra"
          class="form-control"
          required
          name="fk_galaxia_id" value=""
          id="fk_galaxia_id">
      </div>
      <div class="col-md-1">

      </div>

    <br>

      <div class="clo-md-3">
        <label for="">Foto:
            <span class="obligatorio">*</span>
        </label>

        <input type="file" name="foto_recup_id" placeholder="Ingrese el archivo"
        id="foto_recup_id" value="" required>
      </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/planetas/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_planeta").validate({
  rules:{
      nombre_recup_id:{
        required:true,
        minlength:3,
        letras:true
      },
      orden_recup_id:{
        required:true,
        minlength:0,
        digits:true,
      },
      distancia_recup_id:{
        Required:true,
        minlength:3,
        maxlength:50,
        digits:true
      },
      estado_recup_id:{
        required:true,
        minlength:3,
        maxlength:9,
        letras:true
      },
      foto_recup_id:{
        required:true,
      },
  },
  messages:{
    nombre_recup_id:{
      required:"Este campo es requerido",
      minlength:"Ingrese un nombre válido",
      letras: "Este campo solo acepta letras"
    },
    orden_recup_id:{
      required:"Este campo es requerido",
      minlength:"Ingrese un orden válido",
      digits:"Este campo solo acepta números"
    },
    distancia_recup_id:{
      required:"Este campo es requerido",
      minlength:"Ingrese una distancia válida",
      maxlength:"Ingrese una distancia válida",

    },
    estado_recup_id:{
      required:"Este campo es requerido",
      minlength:"Ingrese un estado válido (activo/inactivo)",
      maxlength:"Estado no válido",
    },
    foto_recup_id:{
      required:"Este campo es requerido",
    },
  }
});
</script>
<script type="text/javascript">
$("#foto_recup_id").fileinput({language:'es'});

</script>
