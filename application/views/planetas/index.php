<h1 class="text-center" style="color:white; background-color:gray ; padding:10px 20px"><i class="glyphicon glyphicon-globe"></i> PLANETAS</h1>
<br>

<div class="row">
  &nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/planetas/nuevo" class="btn btn-success">
<i class="glyphicon glyphicon-plus"></i>
Agregar Planeta
  </a>
</div>
<br>

<?php if ($planetas): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:blue ; color:white"id="tbl_planetas">
        <thead>
           <tr>
             <th>ID</th>
             <th>FOTO</th>
             <th>NOMBRE</th>
             <th>ORDEN</th>
             <th>DISTANCIA (KM)</th>
             <th>GALAXIA</th>
             <th>ESTADO</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:#87AFF0;color:black">
           <?php foreach ($planetas as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_recup_id;?></td>
               <td>
               <?php if($filaTemporal->foto_recup_id!=""):?>
                 <img src="<?php echo base_url('uploads/').$filaTemporal->foto_recup_id; ?>" alt="" width="70px" height="70px" style="border-radius:50%">
               <?php else: ?>
                 N/A
               <?php endif; ?>
             </td>
               <td><?php echo $filaTemporal->nombre_recup_id; ?></td>
               <td><?php echo $filaTemporal->orden_recup_id; ?></td>
               <td><?php echo $filaTemporal->distancia_recup_id; ?></td>
               <td><?php echo $filaTemporal->fk_galaxia_id; ?></td>
               <td><?php echo $filaTemporal->estado_recup_id; ?></td>
               <td class="text-center">
               <a href="<?php echo site_url(); ?>/planetas/editar/<?php echo $filaTemporal->id_recup_id;?>" title="Editar">
                 <button type="submit" name="button" class="btn btn-warning">
                   <i class="glyphicon glyphicon-edit" style="color:white"></i>
                   Editar
                 </button>

                 </a>
                 &nbsp; &nbsp; &nbsp;

                   <a href="<?php echo site_url(); ?>/planetas/eliminar/<?php echo $filaTemporal->id_recup_id;?>" title="Eliminar">
                     <button type="submit" name="button" class="btn btn-danger">
                       <i class="glyphicon glyphicon-trash" style="color:white"></i>
                       Eliminar
                     </button>
                   </a>

               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No hay planetas</h1>
       <?php endif; ?>
  </table>


<script type="text/javascript">
  $("#tbl_planetas").dataTable();
</script>
