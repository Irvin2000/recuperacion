<div class="row">
  <div class="col-md-12">
    <h1 class="text-center" style="color:white ; background-color:gray ; padding: 10px 20px"><i class="glyphicon glyphicon-globe"></i> GALAXIAS</h1>
  </div>
</div>

    <a href="<?php echo site_url('galaxias/nuevo'); ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>Agregar Galaxia</a>

<br>
</div>
<br>
<?php if ($galaxias): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_galaxias" style="background-color:#424242 ;color:white">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody style="color:black">
      <?php foreach ($galaxias as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_recup_id ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_recup_id ?>
          </td>
          <td>
            <?php echo $filaTemporal->descripcion_recup_id ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/galaxias/editar/<?php echo $filaTemporal->id_recup_id; ?>" title="Editar Galaxia" ;>
              <button type="submit" name="button" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i>
                   Editar
            </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/galaxias/eliminar/<?php echo $filaTemporal->id_recup_id; ?>" title="Eliminar Galaxia"
            onclick="return confirm('¿Estas seguro de Eliminar de forma permanente ?');"
            style="color:red;">
              <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              Eliminar
            </button>
            </a>

          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay Galaxias </h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_galaxias").DataTable();
</script>
