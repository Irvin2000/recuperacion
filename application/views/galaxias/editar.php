<h1 class="text-center" style="color:white ;background-color:gray"><i class="glyphicon glyphicon-refresh"></i> EDITAR GALAXIA</h1>
<form class=""
id="frm_editar_galaxia"
action="<?php echo site_url('galaxias/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-2">

      </div>
      <input type="hidden" name="id_recup_id" id="id_recup_id" value="<?php echo $galaxiaEditar->id_recup_id; ?>">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre de la galaxia"
          class="form-control"
          required
          name="nombre_recup_id" value="<?php echo $galaxiaEditar->nombre_recup_id; ?>"
          id="nombre_recup_id">
      </div>
      <div class="col-md-4">
          <label for="">Descripción:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese una pequeña descripción de la galaxia"
          class="form-control"
          required
          name="descripcion_recup_id" value="<?php echo $galaxiaEditar->descripcion_recup_id; ?>"
          id="descripcion_recup_id">
      </div>
      <div class="col-md-2">

      </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-save"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/galaxias/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-remove-sign"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>


 <script type="text/javascript">
 $("#frm_editar_galaxia").validate({
   rules:{
     nombre_recup_id:{
       required:true,
       minlength:3,

     },
     descripcion_recup_id:{
       required:true,
       minlength:3,
       maxlength:50,
       letras:true
     }
   },
   messages:{
     nombre_recup_id:{
       required:"Este campo es requerido",
       minlength:"Ingrese un nombre de mas de 3 caracteres",

     },
     descripcion_recup_id:{
       required:"Este campo es requerido",
       minlength:"La descripción debe contener más de 3 caracteres",
       maxlength:"Este campo acepta máximo 50 caracteres"
     }

   }
 });

 </script>
