<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planetas extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model('planeta');

	}

	public function index()
	{
		$data['planetas']=$this->planeta->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('planetas/index',$data);
		$this->load->view('footer')	;
	}
  public function nuevo()
  {
		// $data["listadoGalaxias"]=$this->planeta->obtenerTodos();
    $this->load->view('header')	;
    $this->load->view('planetas/nuevo');
    $this->load->view('footer')	;
  }
	public function guardar()
	{
		$datosNuevoPlaneta=array(
			"nombre_recup_id"=>$this->input->post('nombre_recup_id'),
			"orden_recup_id"=>$this->input->post('orden_recup_id'),
			"distancia_recup_id"=>$this->input->post('distancia_recup_id'),
			"estado_recup_id"=>$this->input->post('estado_recup_id'),
			"fk_galaxia_id"=>$this->input->post('fk_galaxia_id')
		);

		$this->load->library("upload");
				$new_name = "foto_planeta" . time() . "_" . rand(1, 5000);
				$config['file_name'] = $new_name . '_1';
				$config['upload_path']          = FCPATH . 'uploads/';
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 1024*5; //5 MB
				$this->upload->initialize($config);

				if ($this->upload->do_upload("foto_recup_id")) {
					$dataSubida = $this->upload->data();
					$datosNuevoPlaneta["foto_recup_id"] = $dataSubida['file_name'];
				}

	if ($this->planeta->insertar($datosNuevoPlaneta)) {

    redirect('planetas/index');

  }else {
    echo "<h1>ERRORE AL INSERTAR </h1>";
  }

	}
//funcion para Eliminar
	public function eliminar($id_recup_id)
	{
    if ($this->planeta->borrar($id_recup_id)) { //invocando al modelo
    redirect('planetas/index');
  } else {
    echo "ERROR AL BORRAR :C";
  }
		redirect ('planetas/index');

	}
//FUNCION REEDERIZAR VISTA EDITAR
public function editar($id_recup_id){
	$data ["PlanetaEditar"]=$this->planeta->obtenerPorId($id_recup_id);
	$this->load->view('header')	;
	$this->load->view('planetas/editar',$data);
	$this->load->view('footer')	;
}

//PROCESO DE ACTUALIZACION
public function procesarActualizacion(){
	$datosEditados=array(
    "nombre_recup_id"=>$this->input->post('nombre_recup_id'),
    "orden_recup_id"=>$this->input->post('orden_recup_id'),
    "distancia_recup_id"=>$this->input->post('distancia_recup_id'),
    "estado_recup_id"=>$this->input->post('estado_recup_id'),
    "fk_galaxia_id"=>$this->input->post('fk_galaxia_id')
  );

  $this->load->library("upload");
      $new_name = "foto_planeta" . time() . "_" . rand(1, 5000);
      $config['file_name'] = $new_name . '_1';
      $config['upload_path']          = FCPATH . 'uploads/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 1024*5; //5 MB
      $this->upload->initialize($config);

      if ($this->upload->do_upload("foto_recup_id")) {
        $dataSubida = $this->upload->data();
        $datosNuevoPlaneta["foto_recup_id"] = $dataSubida['file_name'];
      }

if ($this->planeta->insertar($datosNuevoPlaneta)) {

  redirect('planetas/index');

}else {
  echo "<h1>ERRORE AL EDITAR </h1>";
}



}
} // cierre de la clase
