<?php

class Galaxias extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('galaxia');
    //LOGIN
     // if(!$this->session->userdata("conectado")){
     //       	redirect("welcome/login");
     //     	}
  }
  //Renderizacion de la vista
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('galaxias/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['galaxias']=$this->galaxia->obtenerTodos();
    $this->load->view('header');
    $this->load->view('galaxias/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevaGalaxia=array(
      "nombre_recup_id"=>$this->input->post('nombre_recup_id'),
      "descripcion_recup_id"=>$this->input->post('descripcion_recup_id')

    );
    // PARA LA SUBIDA DE FOTOS
    // $this->load->library("upload");
    //     $new_name = "foto_instructor_" . time() . "_" . rand(1, 5000);
    //     $config['file_name'] = $new_name . '_1';
    //     $config['upload_path']          = FCPATH . 'uploads/';
    //     $config['allowed_types']        = 'jpeg|jpg|png';
    //     $config['max_size']             = 1024*5; //MB
    //     $this->upload->initialize($config);
    //
    //     if ($this->upload->do_upload("foto_ins")) {
    //       $dataSubida = $this->upload->data();
    //       $datosNuevoInstructor["foto_ins"] = $dataSubida['file_name'];
    //     }

    if($this->galaxia->insertar($datosNuevaGalaxia)){
      redirect('galaxias/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  } //cierre de la funcion guardar
  //funcion para Eliminar
  public function eliminar($id_recup_id){
    //CUANDO TENGA LOGIN -> PERFIL DE USUARIO
    // if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
    //   $this->session->set_flashdata("error","No tiene permisos para eliminar");
    //   redirect("instructores/index");
    // }

     if ($this->galaxia->borrar($id_recup_id)) { //invocando al modelo
     redirect('galaxias/index');
   } else {
     echo "ERROR AL BORRAR :C";
   }
}
  //function renderizar vista editar con el instructor
  public function editar($id_recup_id){
    $data["galaxiaEditar"]=$this->galaxia->obtenerPorId($id_recup_id);
    $this->load->view('header');
    $this->load->view('galaxias/editar',$data);
    $this->load->view('footer');
  }
  //proceso de actualizacion
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_recup_id"=>$this->input->post('nombre_recup_id'),
      "descripcion_recup_id"=>$this->input->post('descripcion_recup_id')

    );
    $id_recup_id=$this->input->post("id_recup_id");
    if ($this->galaxia->actualizar($id_recup_id,$datosEditados)) {
      redirect('galaxias/index');
    } else {
      echo "ERROR AL ACTUALIZAR :C";
    }
 }

}//NO borrar el cierre de la clase


 ?>
